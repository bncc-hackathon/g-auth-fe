import React from "react";

import Home from "assets/images/home.svg";
import Logo from "assets/images/logo.svg";

import {
  HomePageContainer,
  GreetingContent,
  GreetingSubContent,
  GreetingGroup,
  GreetingImage,
  GreetingLogo,
} from "./style";

const HomePage = () => {
  return (
    <HomePageContainer>
      <GreetingGroup>
        <GreetingLogo src={Logo} />
        <GreetingContent>Fast, Secure, and Seamless</GreetingContent>
        <GreetingSubContent>
          Security made simple with our reliable products
        </GreetingSubContent>
      </GreetingGroup>
      <GreetingImage src={Home} />
    </HomePageContainer>
  );
};

export default HomePage;
