import styled from "styled-components";

export const HomePageContainer = styled.div`
  font-family: Noto;
  color: white;
  width: 100vw;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const GreetingGroup = styled.div`
  width: 30%;
  @media (max-width: 968px) {
    width: 70%;
  }
`;

export const GreetingLogo = styled.img``;

export const GreetingContent = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 48px;
  line-height: 65px;
  letter-spacing: 0.08em;
  color: #e33f5a;
`;

export const GreetingSubContent = styled.div`
  margin-top: 20px;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 25px;

  color: #ffffff;
`;

export const GreetingImage = styled.img`
  width: 40%;
  @media (max-width: 968px) {
    display: none;
  }
`;
