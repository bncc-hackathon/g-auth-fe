import styled from "styled-components";
import { COLOR_MAIN } from "constants/color";

export const AdminPageContainer = styled.div`
  display: flex;
  height: 100vh;
`;

export const LeftMenuContainer = styled.div`
  padding: 40px;
  flex: 1;
  background-color: ${COLOR_MAIN};
`;

export const PageContent = styled.div`
  flex: 6;
  background-color: #edf0f9;
`;

export const CompanyLogo = styled.img`
  width: 50%;
`;
