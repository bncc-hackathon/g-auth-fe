import React from "react";

import LeftMenuSection from "components/pages/admin/left-menu-section";

import { AdminPageContainer, PageContent } from "./style";
import AdminNavbar from "components/pages/admin/admin-navbar";
import TokenSection from "components/pages/admin/token-section";

const Admin = () => (
  <AdminPageContainer>
    <LeftMenuSection />
    <PageContent>
      <AdminNavbar />
      <TokenSection />
    </PageContent>
  </AdminPageContainer>
);

export default Admin;
