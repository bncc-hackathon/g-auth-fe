import styled from "styled-components";

import { COLOR_TEXT_MAIN } from "constants/color";

export const ReviewPageContainer = styled.div`
  font-family: Noto;
  width: 100vw;
  display: flex;
  justify-content: center;
  flex-direction: column
  align-items: center;
`;

export const ReviewHeadline = styled.h1`
  color: ${COLOR_TEXT_MAIN};
`;

export const ReviewPictureGroup = styled.div`
  display: flex;
`;

export const ReviewPicture = styled.div`
  width: 125px;
  height: 125px;
  background-color: #c4c4c4;
  margin: 20px;
`;

export const ReviewBox = styled.div`
  width: 50%;
  display: flex;
  justify-content: center;
  flex-direction: column
  align-items: center;
  background: #FFFFFF;
  box-shadow: 0px 4px 10px rgba(97, 123, 227, 0.29);
  border-radius: 10px;
  padding: 40px;
  @media (max-width: 968px) {
    width: 70%;
  }
`;

export const QuoteGroup = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;
export const QuoteLabel = styled.h1``;

export const ReviewText = styled.h2`
  width: 70%;
  text-align: center;
  padding-bottom: 80px;
`;
