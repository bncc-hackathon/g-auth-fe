import React from "react";

import Festage from "assets/images/festage.svg";

import {
  ReviewPageContainer,
  ReviewHeadline,
  ReviewBox,
  QuoteGroup,
  QuoteLabel,
  ReviewText,
  ReviewPictureGroup,
  ReviewPicture,
} from "./style";

const Review = () => {
  return (
    <ReviewPageContainer>
      <ReviewHeadline>Client & Reviews</ReviewHeadline>
      <ReviewPictureGroup>
        <img alt="" src={Festage} />
        <ReviewPicture />
        <ReviewPicture />
      </ReviewPictureGroup>
      <ReviewBox>
        <QuoteGroup>
          <QuoteLabel>"</QuoteLabel>
          <QuoteLabel>"</QuoteLabel>
        </QuoteGroup>
        <ReviewText>
          G-Auth has successfully delivered a wonderful job at providing a
          reliable service for our face verification feature. It has been a
          great pleasure to work with the team.
        </ReviewText>
      </ReviewBox>
    </ReviewPageContainer>
  );
};

export default Review;
