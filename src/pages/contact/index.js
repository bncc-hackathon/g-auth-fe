import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import ContactIllustrator from "assets/images/contact.png";

import {
  ContactPageContainer,
  ContactPageContent,
  ContactPageIllustration,
  ContactTextField,
  GenInTouchHeadline,
  GetInTouchSubLine,
  ContactUsButton,
} from "./style";

const useStyles = makeStyles((theme) =>
  createStyles({
    endAdornment: {
      paddingRight: 0,
      borderRadius: "20px",
    },
  })
);

const Contact = (props) => {
  const classes = useStyles(props);
  return (
    <ContactPageContainer>
      <ContactPageContent>
        <GenInTouchHeadline>Get in Touch</GenInTouchHeadline>
        <GetInTouchSubLine>We’ll keep you updated. No spam!</GetInTouchSubLine>
        <ContactTextField
          id="standard-name"
          variant="outlined"
          placeholder="Your Email"
          InputProps={{
            endAdornment: <ContactUsButton>Send</ContactUsButton>,
            classes: {
              adornedEnd: classes.endAdornment,
            },
          }}
        />
      </ContactPageContent>

      <ContactPageIllustration src={ContactIllustrator} />
    </ContactPageContainer>
  );
};

export default Contact;
