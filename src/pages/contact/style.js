import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { COLOR_TEXT_MAIN } from "constants/color";

export const ContactPageContainer = styled.div`
  font-family: Noto;
  width: 100vw;
  display: flex;
  justify-content: space-around;
  @media (max-width: 968px) {
    flex-direction: column;
    width: 100%;
    align-items: center;
  }
`;

export const ContactPageContent = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GenInTouchHeadline = styled.h1`
  color: ${COLOR_TEXT_MAIN};
  font-style: normal;
  font-weight: bold;
  font-size: 48px;
  line-height: 65px;
  letter-spacing: 0.08em;
  margin: 0px;
`;

export const GetInTouchSubLine = styled.h2`
  margin-top: 4px;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 33px;
`;

export const ContactPageIllustration = styled.img``;

export const ContactUsButton = withStyles({
  root: {
    backgroundColor: COLOR_TEXT_MAIN,
    color: "white",
    padding: "20px 40px",
    borderRadius: "0px 20px 20px 0px",
  },
  adornedEnd: {
    padding: 0,
  },
})(Button);

export const ContactTextField = withStyles({
  root: {
    padding: 0,
    backgroundColor: "#EDF0F9",
    borderRadius: "20px",
  },
})(TextField);
