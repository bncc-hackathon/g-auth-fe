import styled from "styled-components";

export const ProductPageContainer = styled.div`
  font-family: Noto;
  color: white;
  width: 100vw;
  display: flex;
  justify-content: center;
  padding: 0px 100px;
  align-items: flex-start;
  flex-direction: column;
`;

export const ProductPageHeadline = styled.h1`
  color: #e33f5a;
  font-style: normal;
  font-weight: bold;
  font-size: 48px;
  line-height: 65px;
  letter-spacing: 0.08em;
`;

export const ProductListContainer = styled.div`
  width: 60%;
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

export const ProductContainer = styled.div`
  margin: 20px 20px;
`;

export const ProductIcon = styled.img`
  width: 100px;
  height: 100px;
`;

export const ProductLabel = styled.h2``;

export const ProductDescription = styled.h4``;
