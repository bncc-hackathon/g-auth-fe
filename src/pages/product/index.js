import React from "react";

import Authorization from "assets/images/authorization.svg";
import Management from "assets/images/management.svg";
import Spoofing from "assets/images/spoofing.svg";
import Verification from "assets/images/verification.svg";

import {
  ProductPageContainer,
  ProductPageHeadline,
  ProductListContainer,
  ProductContainer,
  ProductIcon,
  ProductLabel,
  ProductDescription,
} from "./style";

const PRODUCTS = [
  {
    icon: Management,
    label: "Authentication Management",
    description:
      "Manage user authentication process with our robust support, reliable security, and delightful user experience",
  },
  {
    icon: Spoofing,
    label: "Anti-Spoofing",
    description:
      "Monitor networks and detect atypical activity using verification methods to prevent spoofing attacks",
  },
  {
    icon: Verification,
    label: "Face Verification",
    description:
      "Provide extra layer of secure protection to your products by face scanning feature",
  },
  {
    icon: Authorization,
    label: "Authorization Management",
    description:
      "Manage user rights in authorization process within all specific service roles based on your needs",
  },
];

const renderProducts = () =>
  PRODUCTS.map((product) => (
    <ProductContainer>
      <ProductIcon src={product.icon} />
      <ProductLabel>{product.label}</ProductLabel>
      <ProductDescription>{product.description}</ProductDescription>
    </ProductContainer>
  ));

const Product = () => (
  <ProductPageContainer>
    <ProductPageHeadline>Our Products</ProductPageHeadline>
    <ProductListContainer>{renderProducts()}</ProductListContainer>
  </ProductPageContainer>
);

export default Product;
