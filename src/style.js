import styled, { createGlobalStyle } from "styled-components";

import Noto from "assets/fonts/Noto/NotoSans-Regular.ttf";

export const PageContainer = styled.div`
  display: table-row;
`;

export const PageContent = styled.div`
  display: table-cell;
  vertical-align: middle;
  height: 100vh;
  width: 100%;
`;

export const GlobalStyleComponent = createGlobalStyle`
  @font-face {
    font-family: Noto;
    src: url(${Noto});
  }
  html, body {
    overflow: hidden;
    margin: 0px;
    padding: 0px;
  }
  ::-webkit-scrollbar {
    width: 0px;
    background: transparent;
    font-family: Noto;
  }
`;
