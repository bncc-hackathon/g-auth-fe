import styled from "styled-components";
import { COLOR_MAIN } from "constants/color";

export const LeftMenuContainer = styled.div`
  flex: 1;
  background-color: ${COLOR_MAIN};
  display: flex;
  color: white;
  font-family: Noto;
  flex-direction: column;
`;

export const CompanyLogo = styled.img`
  width: 50%;
  padding: 40px;
  padding-bottom: 100px;
`;

export const TabGroup = styled.div`
  margin: 10px 20px;
  display: flex;
  padding: 20px;
  align-items: center;
  background-color: ${(props) => props.menu === "Token" && "white"};
  border-radius: 25px;
  color: ${(props) => props.menu === "Token" && COLOR_MAIN};
`;

export const CircleMenu = styled.div`
  width: 50px;
  height: 50px;
  background-color: #c4c4c4;
  border-radius: 50%;
`;

export const TabMenu = styled.div`
  margin-left: 20px;
`;
