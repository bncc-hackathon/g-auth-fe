import React from "react";
import Logo from "assets/images/logo.svg";

import {
  LeftMenuContainer,
  CompanyLogo,
  TabGroup,
  CircleMenu,
  TabMenu,
} from "./style";

const MENUS = ["Dashboard", "User", "Token"];

const renderMenus = () =>
  MENUS.map((menu) => (
    <TabGroup key={menu} menu={menu}>
      <CircleMenu />
      <TabMenu>{menu}</TabMenu>
    </TabGroup>
  ));

const LeftMenuSection = () => (
  <LeftMenuContainer>
    <CompanyLogo src={Logo} />
    {renderMenus()}
  </LeftMenuContainer>
);

export default LeftMenuSection;
