import React from "react";

import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import { COLOR_MAIN } from "constants/color";

import {
  AdminNavbarContainer,
  RightMenu,
  HiLabel,
  NameLabel,
  SearchBar,
} from "./style";

const AdminNavbar = () => (
  <AdminNavbarContainer>
    <SearchBar placeholder="Search . . ." />
    <RightMenu>
      <HiLabel>Hi, </HiLabel>
      <NameLabel>Admin</NameLabel>
      <KeyboardArrowDown fontSize="large" style={{ color: COLOR_MAIN }} />
    </RightMenu>
  </AdminNavbarContainer>
);

export default AdminNavbar;
