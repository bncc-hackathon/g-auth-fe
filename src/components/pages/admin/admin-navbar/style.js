import styled from "styled-components";

import { COLOR_MAIN } from "constants/color";

export const AdminNavbarContainer = styled.div`
  background-color: white;
  padding: 20px 40px;
  font-family: Noto;
  display: flex;
  justify-content: space-between;
`;

export const RightMenu = styled.div`
  display: flex;
  align-items: center;
`;

export const HiLabel = styled.label`
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 19px;
  letter-spacing: 0.08em;

  color: ${COLOR_MAIN};
`;

export const NameLabel = styled.label`
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 19px;
  letter-spacing: 0.08em;

  color: ${COLOR_MAIN};
  margin-right: 12px;
`;

export const SearchBar = styled.input`
  width: 20%;
  padding: 12px;
  border-radius: 10px;
  background-color: #edf0f9;
  border: none;
  &:focus {
    outline: none;
    box-shadow: none;
  }
`;
