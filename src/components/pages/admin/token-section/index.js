import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import Api from "api";

import BasicTable from "components/common/table";

import {
  AddButton,
  SectionHeadlineGroup,
  SectionLabel,
  TokenSectionContainer,
  ModalHeading,
  InputGroup,
  InputLabel,
  InputValue,
} from "./style";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    outline: 0,
  },
  paper: {
    width: "20%",
    fontFamily: "Noto",
    outline: 0,
    borderRadius: "10px",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function createData(no, name, token, status, createdDate) {
  return { no, name, token, status, createdDate };
}

const headings = ["No", "Name", "Token", "Status", "Created Date", "Action"];

const TokenSection = () => {
  const [rows, setRows] = React.useState([]);
  const [tokenName, setTokenName] = React.useState("");
  const [date, setDate] = React.useState("");
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const fetchTokens = React.useCallback(async () => {
    const { data } = await Api.get("/api/admin/connection/all");
    const result = data.data.map((datum) =>
      createData(
        datum.id,
        datum.name,
        datum.token,
        datum.is_valid ? "ACTIVE" : "INACTIVE",
        datum.created_at
      )
    );
    setRows(result);
  }, []);

  useEffect(() => {
    fetchTokens();
  }, [fetchTokens]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async () => {
    console.log(date);
    await Api.post("/api/admin/connection/generate", {
      name: tokenName,
    });
    fetchTokens();
    handleClose();
  };

  return (
    <TokenSectionContainer>
      <SectionHeadlineGroup>
        <SectionLabel>Token Management</SectionLabel>
        <AddButton onClick={handleOpen}>Add Token</AddButton>
      </SectionHeadlineGroup>
      {rows.length !== 0 && (
        <BasicTable fetchTokens={fetchTokens} headings={headings} rows={rows} />
      )}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalHeading>Add New Token</ModalHeading>
            <InputGroup>
              <InputLabel>Token Name</InputLabel>
              <InputValue
                onChange={(e) => setTokenName(e.target.value)}
                placeholder="Enter Token Here"
              />
            </InputGroup>
            <InputGroup>
              <InputLabel>Expiration Date</InputLabel>
              <InputValue
                onChange={(e) => setDate(e.target.value)}
                placeholder="dd/mm/yy"
                type="date"
              />
            </InputGroup>
            <AddButton
              onClick={handleSubmit}
              style={{ width: "100%", margin: "20px 0px" }}
            >
              Add
            </AddButton>
          </div>
        </Fade>
      </Modal>
    </TokenSectionContainer>
  );
};

export default TokenSection;
