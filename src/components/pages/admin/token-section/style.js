import styled from "styled-components";
import { COLOR_MAIN } from "constants/color";

export const TokenSectionContainer = styled.div`
  font-family: Noto;
  padding: 40px;
`;

export const SectionHeadlineGroup = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const AddButton = styled.button`
  padding: 10px 40px;
  border-radius: 10px;
  background-color: ${COLOR_MAIN};
  color: white;
`;

export const SectionLabel = styled.h2``;

export const ModalHeading = styled.h3``;

export const InputGroup = styled.div`
  display: flex;
  flex-direction: column;
`;

export const InputLabel = styled.label``;

export const InputValue = styled.input`
  margin: 10px 0px;
  padding: 12px;
  border-radius: 10px;
  background-color: #edf0f9;
  border: none;
  &:focus {
    outline: none;
    box-shadow: none;
  }
`;
