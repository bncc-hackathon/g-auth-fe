import styled from "styled-components";

export const RevokeButton = styled.button`
  font-family: Noto;
  background-color: #e33f5a;
  color: white;
  padding: 2px 8px;
  border-radius: 4px;
  font-weight: bolder;
  border: none;
`;
