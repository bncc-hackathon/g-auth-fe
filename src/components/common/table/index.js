import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import { RevokeButton } from "./style";

import Api from "api";

const useStyles = makeStyles({
  tableContainer: {
    padding: "20px 40px 80px 40px",
    width: "94%",
    borderRadius: "10px",
  },
  table: {
    minWidth: "100%",
  },
});

const BasicTable = ({ rows, headings, fetchTokens }) => {
  const classes = useStyles();

  const revokeApi = async (id) => {
    await Api.post("/api/admin/connection/revoke", {
      id,
    });
    fetchTokens();
  };

  return (
    <TableContainer className={classes.tableContainer} component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {headings.map((heading) => (
              <TableCell>{heading}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.no}
              </TableCell>
              <TableCell align="left">{row.name}</TableCell>
              <TableCell align="left">{row.token}</TableCell>
              <TableCell align="left">{row.status}</TableCell>
              <TableCell align="left">{row.createdDate}</TableCell>
              <TableCell align="left">
                <RevokeButton onClick={() => revokeApi(row.no)}>X</RevokeButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default BasicTable;
