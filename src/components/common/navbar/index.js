import React from "react";
import { useHistory } from "react-router-dom";

import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import { HiLabel, NavbarContainer, NameLabel } from "./style";

const Navbar = () => {
  const history = useHistory();
  return (
    <NavbarContainer onClick={() => history.push("/admin")}>
      <HiLabel>Hi, </HiLabel>
      <NameLabel>Admin</NameLabel>
      <KeyboardArrowDown fontSize="large" style={{ color: "white" }} />
    </NavbarContainer>
  );
};

export default Navbar;
