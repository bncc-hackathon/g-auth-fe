import styled from "styled-components";

export const NavbarContainer = styled.div`
  font-family: Noto;
  position: absolute;
  background-color: transparent;
  z-index: 999;
  top: 40px;
  right: 40px;
  display: flex;
  align-items: center;
  cursor: pointer;
`;

export const HiLabel = styled.label`
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 19px;
  letter-spacing: 0.08em;

  color: #ffffff;
`;

export const NameLabel = styled.label`
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 19px;
  letter-spacing: 0.08em;

  color: #e33f5a;
  margin-right: 12px;
`;
