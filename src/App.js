import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Home from "pages/home";
import ScrollableContainer from "./components/common/scrollable-container";
import Navbar from "./components/common/navbar";

import { COLOR_MAIN } from "constants/color";
import { PageContainer, PageContent } from "./style";
import Contact from "./pages/contact";
import Review from "./pages/review";
import Product from "./pages/product";
import Admin from "./pages/admin";

const PAGE = [
  {
    component: <Home />,
    backgroundColor: COLOR_MAIN,
  },
  {
    component: <Product />,
    backgroundColor: COLOR_MAIN,
  },
  {
    component: <Review />,
    backgroundColor: "white",
  },
  {
    component: <Contact />,
    backgroundColor: "white",
  },
];

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/admin" component={Admin} />
        <Route path="/">
          <Navbar />
          <ScrollableContainer>{renderPage()}</ScrollableContainer>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

const renderPage = () =>
  PAGE.map((p, index) => (
    <PageContainer key={index}>
      <PageContent style={{ backgroundColor: p.backgroundColor }}>
        {p.component}
      </PageContent>
    </PageContainer>
  ));

export default App;
